/* Copyright (C) 2013 Timothée Giet <animtim@gmail.com>
 * Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
 * Contact: http://www.qt-project.org/legal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


import QtQuick 2.0

Item {
    id:slider
    property real min:0
    property real max:1
    property real value: min + (max - min) * (bar.x / (foo.width - bar.width))
    property real init:  min+(max-min)
    property string name:"Slider"

    Component.onCompleted: setValue(init)
    function setValue(v) {
       if (min < max)
          bar.x = v/(max - min) * (foo.width - bar.width);
    }
    Rectangle {
        id:sliderName
        anchors.left:parent.left
        height: childrenRect.height
        width:childrenRect.width
        anchors.verticalCenter:parent.verticalCenter
        color: "transparent"
        Text {
           text:slider.name
           font.pointSize:11
           color: blueText
         }
    }
    Item {
        id: foo
        height: 6
        width: parent.width - 4 - sliderName.width
        anchors.verticalCenter:parent.verticalCenter
        anchors.left:sliderName.right
        anchors.leftMargin:5

        Rectangle {
            height: parent.height
            anchors.left: parent.left
            anchors.right: bar.horizontalCenter
            color: "#4250C8"
            radius: 3
        }
        Rectangle {
            height: parent.height
            anchors.left: bar.horizontalCenter
            anchors.right: parent.right
            color: "gray"
            radius: 3
        }
        Rectangle {
            anchors.fill: parent
            color: "transparent"
            radius: 3
            border.width: 1
            border.color: "#626C70"
            Rectangle{
                id:scrollArea
                width: parent.width * 2
                height: parent.height * 4
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                MouseArea {
                    anchors.fill: parent
                    onWheel: {
                        if(wheel.angleDelta.y > 0)
                            bar.x = bar.x + 5;
                        else
                            bar.x = bar.x - 5;
                        if(bar.x > foo.width-bar.width)
                            bar.x = foo.width-bar.width;
                        if(bar.x < 0)
                            bar.x = 0;
                    }

                }
            }
        }

        Rectangle {
            id: bar
            y: -7
            width: 20
            height: 20
            radius: 15
            border.width: 1
            border.color: "#626C70"
            gradient: Gradient {
                GradientStop {
                    position: 0.1
                    color: "#DDDDDD"
                }
                GradientStop {
                    position: 0.9
                    color: "#46545C"
                }
                GradientStop {
                    position: 1
                    color: "#72787C"
                }
            }
            MouseArea {
                anchors.fill: parent
                drag.target: parent
                drag.axis: Drag.XAxis
                drag.minimumX: 0
                drag.maximumX: foo.width - parent.width
            }
        }
    }
}
