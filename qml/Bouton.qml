/* Copyright (C) 2013 Timothée Giet <animtim@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0

Rectangle {
    width: 80
    height: 42
    radius: 15
    border.width: 1
    border.color: "#626C70"
    gradient: Gradient {
        GradientStop {
            position: 0.00
            color: "#B8B0B5"
        }
        GradientStop {
            position: 0.1
            color: "#99A0A4"
        }
        GradientStop {
            position: 0.9
            color: "#46545C"
        }
        GradientStop {
            position: 1
            color: "#72787C"
        }
    }
}
