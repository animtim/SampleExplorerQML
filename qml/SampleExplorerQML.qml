/* Copyright (C) 2013 Timothée Giet <animtim@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.0
import QtMultimedia 5.0
import Qt.labs.folderlistmodel 2.1
import QtQuick.Dialogs 1.2


Rectangle {
    id: mainWindow
    width: 800
    height: 600
    radius: 15
    property string blueText: "#2D3242"
    property string selectedFile : ""
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#374051"
        }

        GradientStop {
            position: 0.74
            color: "#4a595e"
        }

        GradientStop {
            position: 0.97
            color: "#72787c"
        }
    }
    border.color: "white"

    
    FileDialog {
        id: fileDialog
    title: "Please select a folder"
    selectFolder: true
    nameFilters: ["*.wav", "*.flac", "*.mp3"]
    onAccepted: {
        console.log("You selected: " + fileDialog.folder)
        folderModel.folder = fileDialog.folder
    }
    onRejected: {
        console.log("Canceled")
    }
    Component.onCompleted: visible = true
}

    
    Text {
        id: text1
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Sample Explorer QML"
        font.family: "TSCu_Comic"
        font.pointSize: 23
        color: "white"
        horizontalAlignment: Text.AlignHCenter
    }

    Bouton {
        id: selectDir
        anchors.leftMargin: 10
        anchors.topMargin: 10
        anchors.top: parent.top
        anchors.left: parent.left
        Text {
            anchors.centerIn: parent
            text: "Move On"
            color: "white"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {

                fileDialog.open()
            }
        }
    }

    Rectangle{
        id: list
        width: (parent.width/2-13); height: (parent.height-100)
        radius: 6
        color: "#7Bffffff"
        anchors.leftMargin: 10
        anchors.topMargin: 60
        anchors.top: parent.top
        anchors.left: parent.left

        Rectangle{
            id:clipText
            width: parent.width - 8
            height: parent.height -8
            radius: 5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: Qt.rgba(1,1,1,0.8)
                }

                GradientStop {
                    position: 1
                    color: Qt.rgba(1,1,1,0)
                }
            }

        ListView {
            id:browseList
            anchors.fill: parent
            clip: parent
            highlight: Rectangle { color: "lightsteelblue"; width: parent.width; radius: 4 }
            focus: true
            currentIndex: 0

            FolderListModel {
                id: folderModel
                showDotAndDotDot: false
                showDirs: false
                folder : fileDialog.folder
                nameFilters: ["*.wav", "*.flac", "*.mp3"]
            }

               Component {
                id: fileDelegate
                  Text {
                      text: fileName
                      font.pointSize: 11
                      color: blueText
                      focus: true
                      Keys.onUpPressed: {
                          browseList.decrementCurrentIndex()
                          playSample.stop()
                      }

                      Keys.onDownPressed: {
                          browseList.incrementCurrentIndex()
                          playSample.stop()
                      }

                      Keys.onRightPressed: {
                          playSample.stop()
                      }

                      Keys.onReleased: {
                          text01.text = fileName
                          playSample.play()
                      }


                      MouseArea {
                          anchors.fill: parent
                          onClicked: {
                              playSample.stop()
                              browseList.currentIndex = index
                              text01.text = parent.text
                              playSample.play()
                          }
                          onWheel: {
                              if(wheel.angleDelta.y < -30)
                                 browseList.incrementCurrentIndex()
                              if(wheel.angleDelta.y > 30)
                                  browseList.decrementCurrentIndex()
                          }
                      }
                  }
              }

            model: folderModel
            delegate: fileDelegate
          }
       }
    }

    Bouton {
        anchors.rightMargin: 10
        anchors.bottomMargin: 10
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        Text {
            anchors.centerIn: parent
            text: "Quit"
            color: "white"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                Qt.quit()
            }
       }
    }

    Rectangle {
        width: (parent.width/2-13); height: (parent.height-100)
        radius: 6
        color: "#7Bffffff"
        anchors.rightMargin: 10
        anchors.topMargin: 40
        anchors.top: parent.top
        anchors.right: parent.right

        Rectangle {
            id:clipTitle
            radius: 5
            width: parent.width - 8
            height: 42
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 4
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: Qt.rgba(1,1,1,0.42)
                }

                GradientStop {
                    position: 0.3
                    color: Qt.rgba(1,1,1,0.23)
                }
            }

          Text {
            id:text01
            text: "File Name"
            color: blueText
            font.pointSize: 23
            width: parent.width - 8
            elide: Text.ElideRight
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 4

              Audio {
                id: playSample
                source: (fileDialog.folder + '/' + text01.text)
                volume: volSon.value
              }
              MouseArea {
                id: playArea
                anchors.fill: parent
                onPressed:  { playSample.stop() }
              }
          }
        }


        Rectangle {
            id:clipTime
            radius: 5
            width: parent.width - 8
            height: 42
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: clipTitle.bottom
            anchors.topMargin: 4
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: Qt.rgba(1,1,1,0.42)
                }

                GradientStop {
                    position: 0.3
                    color: Qt.rgba(1,1,1,0.23)
                }
            }

          Text {
              id:durationLabel
            text: "Duration: " + (playSample.duration / 1000) + " s."
            color: blueText
            anchors.top: parent.top
            anchors.topMargin: 4
            anchors.left: parent.left
            anchors.leftMargin: 4
            width: parent.width - 8
            elide: Text.ElideRight
          }

            Text {
                text: "PlayTime: " + (playSample.position / 1000) + " s."
                color: blueText
                anchors.top: durationLabel.bottom
                anchors.topMargin: 4
                anchors.left: parent.left
                anchors.leftMargin: 4
                width: parent.width - 8
                elide: Text.ElideRight
            }
        }

        Slider {
            id: volSon
            name: "Volume "
            width: 200
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 20
            anchors.bottom: parent.bottom
        }
    }
}
